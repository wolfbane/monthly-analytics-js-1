var gulp   = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var config = require('../config');
var handleErrors = require('../util/handleErrors');

gulp.task('copy', function() {
  return gulp.src(config.env.tmp + config.js.path + '/monthly-analytics.js')
    .pipe(gulp.dest(config.env.dst + config.js.path));
});

gulp.task('uglify', function() {
  return gulp.src(config.env.tmp + config.js.path + '/monthly-analytics.js')
    .on('error', handleErrors)
    .pipe(uglify())
    .pipe(rename("monthly-analytics.min.js"))
    .pipe(gulp.dest(config.env.dst + config.js.path));
});
